! 1 
! 1 # 1 "pointer.c"
! 1 # 6 "/usr/lib/bcc/include/asm/types.h"
! 6 	
! 7 
! 8 
! 9 typedef unsigned char __u8;
!BCC_EOS
! 10 typedef unsigned char * __pu8;
!BCC_EOS
! 11 # 17
! 17 typedef unsigned short __u16;
!BCC_EOS
! 18 typedef unsigned short * __pu16;
!BCC_EOS
! 19 typedef short __s16;
!BCC_EOS
! 20 typedef short * __ps16;
!BCC_EOS
! 21 
! 22 typedef unsigned long __u32;
!BCC_EOS
! 23 typedef unsigned long * __pu32;
!BCC_EOS
! 24 typedef long __s32;
!BCC_EOS
! 25 typedef long * __ps32;
!BCC_EOS
! 26 
! 27 
! 28 
! 29 typedef unsigned int __uint;
!BCC_EOS
! 30 typedef int __sint;
!BCC_EOS
! 31 typedef unsigned int * __puint;
!BCC_EOS
! 32 typedef int * __psint;
!BCC_EOS
! 33 # 6 "/usr/lib/bcc/include/linuxmt/types.h"
! 6 typedef __u32 off_t;
!BCC_EOS
! 7 typedef __u16 pid_t;
!BCC_EOS
! 8 typedef __u16 uid_t;
!BCC_EOS
! 9 typedef __u16 gid_t;
!BCC_EOS
! 10 typedef __u32 time_t;
!BCC_EOS
! 11 typedef __u16 umode_t;
!BCC_EOS
! 12 typedef __u16 nlink_t;
!BCC_EOS
! 13 typedef __u16 mode_t;
!BCC_EOS
! 14 typedef __u32 loff_t;
!BCC_EOS
! 15 typedef __u32 speed_t;
!BCC_EOS
! 16 typedef __u16 size_t;
!BCC_EOS
! 17 
! 18 typedef __u16 dev_t;
!BCC_EOS
! 19 typedef __uint ino_t;
!BCC_EOS
! 20 typedef __u32 tcflag_t;
!BCC_EOS
! 21 typedef __u8  cc_t;
!BCC_EOS
! 22 
! 23 typedef int   ptrdiff_t;
!BCC_EOS
! 24 # 41 "/usr/lib/bcc/include/stdio.h"
! 41 struct __stdio_file {
! 42   unsigned char *bufpos;   
!BCC_EOS
! 43   unsigned char *bufread;  
!BCC_EOS
! 44   unsigned char *bufwrite; 
!BCC_EOS
! 45   unsigned char *bufstart; 
!BCC_EOS
! 46   unsigned char *bufend;
!BCC_EOS
! 47    
! 48 
! 49   int fd; 
!BCC_EOS
! 50   int mode;
!BCC_EOS
! 51 
! 52   char unbuf[8];	   
!BCC_EOS
! 53 
! 54   struct __stdio_file * next;
!BCC_EOS
! 55 };
!BCC_EOS
! 56 # 62
! 62 typedef struct __stdio_file FILE;
!BCC_EOS
! 63 # 70
! 70 extern FILE stdin[1];
!BCC_EOS
! 71 extern FILE stdout[1];
!BCC_EOS
! 72 extern FILE stderr[1];
!BCC_EOS
! 73 # 102
! 102 extern int setvbuf () ;
!BCC_EOS
! 103 
! 104 
! 105 
! 106 extern void setbuffer () ;
!BCC_EOS
! 107 
! 108 extern int fgetc () ;
!BCC_EOS
! 109 extern int fputc () ;
!BCC_EOS
! 110 
! 111 extern int fclose () ;
!BCC_EOS
! 112 extern int fflush () ;
!BCC_EOS
! 113 extern char *fgets () ;
!BCC_EOS
! 114 
! 115 extern FILE *fopen () ;
!BCC_EOS
! 116 extern FILE *fdopen () ;
!BCC_EOS
! 117 extern FILE *freopen  () ;
!BCC_EOS
! 118 # 123
! 123 extern int fputs () ;
!BCC_EOS
! 124 extern int puts () ;
!BCC_EOS
! 125 
! 126 extern int printf () ;
!BCC_EOS
! 127 extern int fprintf () ;
!BCC_EOS
! 128 extern int sprintf () ;
!BCC_EOS
! 129 # 2 "pointer.c"
! 2 int main(){
export	_main
_main:
! 3  printf("%d",(int)sizeof(long));
push	bp
mov	bp,sp
push	di
push	si
! Debug: list int = const 4 (used reg = )
mov	ax,*4
push	ax
! Debug: list * char = .1+0 (used reg = )
mov	bx,#.1
push	bx
! Debug: func () int = printf+0 (used reg = )
call	_printf
add	sp,*4
!BCC_EOS
! 4  return(0);
xor	ax,ax
pop	si
pop	di
pop	bp
ret
!BCC_EOS
! 5 }
! 6 
! Register BX used in function main
.data
.1:
.2:
.ascii	"%d"
.byte	0
.bss

! 0 errors detected
