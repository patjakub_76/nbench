
BYTEmark* Native Mode Benchmark ver. 2 (10/95)
Original AMD K6-233 index by Andrew D. Balsa (11/97)
Pentium III 1GHz index by Patryk Jakubowski (01/2007)
Linux/Unix* port by Uwe F. Mayer (12/96,11/97)

TEST                : Iterations/sec.  : Orig Index  : New Index
                    :                  : AMD K6/233* : PIII 1GHz*
--------------------:------------------:-------------:------------
NUMERIC SORT        :          2275.2  :      19.16  :       4.96
STRING SORT         :          164.52  :      11.38  :       3.83
BITFIELD            :      5.6852e+08  :      20.37  :       3.32
FP EMULATION        :           189.4  :      20.97  :       2.43
FOURIER             :           16962  :      10.83  :       1.74
ASSIGNMENT          :          30.404  :      30.01  :       3.16
IDEA                :          4671.5  :      21.21  :       4.03
HUFFMAN             :          2209.3  :      19.56  :       3.05
NEURAL NET          :          39.023  :      26.37  :       3.33
LU DECOMPOSITION    :          1714.1  :      64.12  :       3.73
==========================ORIGINAL AMD K6/233 RESULTS========================
INTEGER INDEX       : 19.721
FLOATING-POINT INDEX: 26.361
Baseline (LINUX*)   : AMD K6/233, 512 KB L2-cache, gcc 2.7.2, libc-5.4.38
==============================LINUX DATA BELOW===============================
CPU                 : 
L2 Cache            : 
OS                  : 
MEMORY INDEX        : 3.426
INTEGER INDEX       : 3.490
FLOATING-POINT INDEX: 2.786
Baseline (LINUX)    : Pentium III 1GHz (133MHz FSB), 512 KB L2-cache, gcc 4.1.2, libc6 2.3.6
* Trademarks are property of their respective holder.
